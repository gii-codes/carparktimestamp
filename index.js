const express = require('express')
const req = require('express/lib/request')
const env = require('./env')
const route = require('./src/route')

let app = express()
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
app.use('/assets', express.static('assets'))
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
})
route.set(app)
app.use((err, req, res, next) => {
    console.error(err)
    res.status(500).json({
        error: err
    })
})
let server = require('http').createServer(app)
server.listen(env.port)
console.log(`http://${env.host}:${env.port}`)
const moment = require('moment')



module.exports = {
    parseDate(stringDate) {
        let date = moment(stringDate, "DD-MM-YYYY HH:mm:ss", true);
        if (!date.isValid()) throw 'รูปแบบวันที่ไม่ถูกต้อง'
        return date.toDate()
    },
    format(date){
        if (date === undefined || date === null || date === '') 
            return null
        return moment(date).format('DD-MM-YYYY HH:mm:ss')
    }
}
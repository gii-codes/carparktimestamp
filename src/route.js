const env = require('../env')
const path = require("path")
const fs = require('fs')
const util = require('./util')
const moment = require('moment')
const model = require('./model')
const { Car } = require('./model')
const asyncHandler = fn => (req, res, next) => {
    return Promise
        .resolve(fn(req, res, next))
        .catch(next);
};

model.sync().catch((e) => {
    console.log(e)
});

module.exports = {
    set(app) {
        app.get('/', (req, res) => {
            res.sendFile(path.join(__dirname, 'pages', 'home', 'index.html'));
            // res.sendFile(path.join(__dirname, 'html', 'index.html'));
        })

        app.get('/api/car', asyncHandler(async (req, res) => {
            let page = req.query.page
            if (!page) page = 1
            const size = 5
            const cars = await Car.findAll({
                limit: size,
                offset: (page - 1) * size
            });
            res.send({
                total: await Car.count(),
                page: page,
                size: size,
                data: cars.map((c)=>c.toJson())
            })
        }))

        app.get('/api/car/:id', asyncHandler(async (req, res) => {
            if (!req.params.id) {
                res.status(404).send('data not found')
                return
            }
            const car = await Car.findOne({
                where: {id: req.params.id}
            });
            if (car === null) {
                res.status(404).send('data not found')
                return
            }
            res.send(car.toJson())
        }))

        app.put('/api/car/', asyncHandler (async (req, res) => {
            let json = req.body
            if (!json.licenseNumber) throw 'ไม่ได้รับ licenseNumber'
            if (!json.timeIn) throw 'ไม่ได้รับ timeIn'
            json.date = util.parseDate(json.timeIn)
            await Car.create({licenseNumber : json.licenseNumber, timeIn: json.date})
            res.json({success: true})
        }))

        app.post('/api/car/', asyncHandler (async (req, res) => {
            let json = req.body
            if (!json.id) throw 'ไม่ได้รับ id'
            if (!json.timeOut) throw 'ไม่ได้รับ timeOut'
            json.date = util.parseDate(json.timeOut)
            await Car.update({ timeOut:  json.date }, {where: {id: json.id}});
            res.json({success: true})
        }))

        app.post('/api/car/create', asyncHandler (async (req, res) => {
            const cars = await Car.findAll();
            if (cars.length > 0) {
                res.json({success: true})
                return;
            }
            await Car.create({ licenseNumber: "กข-1111", timeIn: Date.now() });
            await Car.create({ licenseNumber: "กศ-3222", timeIn: Date.now() });
            await Car.create({ licenseNumber: "บจ-2432", timeIn: Date.now() });
            await Car.create({ licenseNumber: "อพ-2873", timeIn: Date.now() });
            await Car.create({ licenseNumber: "วล-9044", timeIn: Date.now() });
            await Car.create({ licenseNumber: "นน-7744", timeIn: Date.now() });
            await Car.create({ licenseNumber: "พม-2231", timeIn: Date.now() });
            res.json({success: true})
        }))
    }
}
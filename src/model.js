const util = require('./util')

const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.db'
});

class Car extends Model {

    toJson() {
        return {
            id: this.id,
            licenseNumber: this.licenseNumber,
            timeIn: util.format(this.timeIn),
            timeOut: util.format(this.timeOut),
        }
    }
}

Car.init( {
    licenseNumber: {
        type: DataTypes.STRING,
        allowNull: false
    },
    timeIn: {
        type: DataTypes.DATE,
        allowNull: false
    },
    timeOut: {
        type: DataTypes.DATE
    }
}, {
  sequelize, // We need to pass the connection instance
  modelName: 'Car', // We need to choose the model name,
  freezeTableName: true
});

// const Car = sequelize.define('Car', {
//     licenseNumber: {
//         type: DataTypes.STRING,
//         allowNull: false
//     },
//     timeIn: {
//         type: DataTypes.DATE,
//         allowNull: false
//     },
//     timeOut: {
//         type: DataTypes.DATE
//     },
// }, {
//     freezeTableName: true
// });

module.exports.Car = Car
module.exports.sync = async () => {
    try {
        sequelize.sync()
    } catch (e) {
        console.log(e)
    }
}
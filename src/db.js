const { Sequelize } = require('sequelize');

// Option 2: Passing parameters separately (sqlite)
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.db'
});

module.exports = {
    sync() {
        try {
            sequelize.sync()
        } catch (error) {
            console.error(error);
        }
    }
}
